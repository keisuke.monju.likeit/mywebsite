package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBmanager;
import beans.MakerBeans;

/**
 * makerテーブル用のDAO
 * @author monju
 */

public class MakerDao {

	/**
	 * 国産メーカーを取得するメソッド
	 */
	public static ArrayList<MakerBeans> findJpnMaker() throws SQLException {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT * FROM carec_db.maker WHERE country='国産車'";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<MakerBeans> jpnMakerList = new ArrayList<MakerBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をcarListにadd()する
			while (rs.next()) {
				MakerBeans maker = new MakerBeans();
				maker.setId(rs.getInt("id"));
				maker.setMakerName(rs.getString("maker_name"));
				jpnMakerList.add(maker);

			}
			return jpnMakerList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	* 輸入メーカーを取得するようメソッド
	*/
	public static ArrayList<MakerBeans> findOutMaker() throws SQLException {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT * FROM carec_db.maker WHERE country='輸入車'";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<MakerBeans> outMakerList = new ArrayList<MakerBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をcarListにadd()する
			while (rs.next()) {
				MakerBeans maker = new MakerBeans();
				maker.setId(rs.getInt("id"));
				maker.setMakerName(rs.getString("maker_name"));
				outMakerList.add(maker);

			}
			return outMakerList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

/**
 * idに紐づいたメーカー名を取得するメソッド
 * @param id
 * @return
 */
	public static MakerBeans getMakerName(int id) {
		MakerBeans mb = new MakerBeans();
	    Connection conn = null;
	        try {
	            // データベースへ接続
				conn = DBmanager.getConnection();

				// TODO ここに処理を書いていく
				//SQL確認済み
				String sql ="SELECT * FROM maker WHERE id =?";
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1,id);
				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					mb.setId(rs.getInt("id"));
					mb.setMakerName(rs.getString("maker_name"));

				}

			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
					if (conn != null) {
			          try {
			                conn.close();
			           } catch (SQLException e) {
			                e.printStackTrace();
						}
					}
			}
	        return mb;
	}

}
