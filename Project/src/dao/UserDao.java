package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBmanager;
import beans.UserBeans;
import util.MyUtil;

/**
 * userテーブル用のDAO
 * @author monju
 */

public class UserDao {

/**
 * ログイン時にlogin_idとpasswordが存在するか確認用
 * @param loginId
 * @param password
 */
	public UserBeans findByLoginInfo(String loginId, String password) {
	    Connection conn = null;
	        try {
	            // データベースへ接続
	  conn = DBmanager.getConnection();

	 // TODO ここに処理を書いていく
	 //SQL確認済み
	  String sql = "SELECT * FROM user WHERE login_id=? and login_password=?";

	 //SELECTを実行して、結果を取得
	  PreparedStatement pStmt = conn.prepareStatement(sql);
	  pStmt.setString(1,loginId);
	  String result = MyUtil.passcode(password);
	  pStmt.setString(2,result);
	  ResultSet rs = pStmt.executeQuery();

	 //ログイン失敗時の処理
	  if(!rs.next()){
		  return null;
	  }

	 //ログイン成功時の処理
	  String loginIdDate = rs.getString("login_id");
	  String userNameDate = rs.getString("user_name");
	  return new UserBeans(loginIdDate,userNameDate);

	} catch (SQLException e) {
	        	e.printStackTrace();
	        	return null;
	} finally {
	 // データベース切断
	          if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
	        	}
	}
	}

	/**
	 * 新規登録時の情報追加用
	 */

	public UserBeans useNewReg(String loginId,String password,String name,
			String birthday,String address,String cardNo,String cardLimitMM,String cardLimitYY,String maker) {
	    Connection conn = null;
        try {
            // データベースへ接続
			 conn = DBmanager.getConnection();

			 // TODO ここに処理を書いていく
			 //SQL確認済み
			 /**任意項目が未入力の際、処理がうまく行くか不明*/
			 String sql = "INSERT INTO "
			 		+ "user(login_id,login_password,user_name,birth_date,address,card_no,card_limitMM,card_limitYY,maker_id)"
			 		+ "VALUE"
			 		+ "(?,?,?,?,?,?,?,?,?)";

			 PreparedStatement pStmt = conn.prepareStatement(sql);
			 pStmt.setString(1,loginId);
			 String result1 = MyUtil.passcode(password);
			 pStmt.setString(2,result1);
			 pStmt.setString(3,name);
			 pStmt.setString(4,birthday);
			 pStmt.setString(5,address);
			 pStmt.setString(6,cardNo);
			 pStmt.setString(7,cardLimitMM);
			 pStmt.setString(8,cardLimitYY);
			 pStmt.setString(9,maker);
			 pStmt.executeUpdate();


			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
			          if (conn != null) {
			          	try {
			                		conn.close();
			            	} catch (SQLException e) {
			                		e.printStackTrace();
			                		return null;
			            	}
			        	}
			}
        		return null;
			}

/**
 * 新規登録時のログインIDが既に
 * 存在するか確認用
 */

	public UserBeans idCheck(String loginID) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdDate = rs.getString("login_id");
			return new UserBeans(loginIdDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	public static int getId(String loginId) throws SQLException{
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT id FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return 0;
			}

			return rs.getInt("id");

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}

	}


}
