package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBmanager;
import beans.BuyBeans;

/**
 * buyテーブル用のDAO
 * @author k_monju
 *
 */

public class BuyDao {

	/**
	 * 購入情報を登録するメソッド
	 * @param userId
	 * @param carId
	 * @param deliveryPrice
	 * @param totalPrice
	 * @param color
	 * @return
	 */

	public static BuyBeans newBuyDate(int userId,String carId,int deliveryPrice,int totalPrice,String color) {
		BuyBeans bb = new BuyBeans();
		Connection conn =null;
        try {
            // データベースへ接続
			conn = DBmanager.getConnection();
			//SQL確認済み
			String sql = "INSERT INTO buy(user_id,car_id,date,delivery_price,total_price,color) VALUES(?,?,now(),?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, userId);
			pStmt.setString(2, carId);
			pStmt.setInt(3, deliveryPrice);
			pStmt.setInt(4, totalPrice);
			pStmt.setString(5, color);
			pStmt.executeUpdate();


		} catch (SQLException e) {
		        	e.printStackTrace();
		        	return null;
		} finally {
		 // データベース切断
				if (conn != null) {
		          try {
		                conn.close();
		           } catch (SQLException e) {
		                e.printStackTrace();
		                return null;
					}
				}
		}
        return bb;
	}

/**
 * 購入履歴のデータ取得用
 * @param id
 * @return
 * @throws SQLException
 */

	public static ArrayList<BuyBeans> getBuyHistory(int id) throws SQLException{
	    Connection conn = null;
        try {
        // データベースへ接続
		conn = DBmanager.getConnection();

		//SQL確認済み
		String sql ="SELECT t.id,t.date,t.delivery_price,t.total_price,t.color,t1.car_name,t1.file_name,t1.detail,t1.price"
				+ " FROM buy t"
				+ " INNER JOIN car t1"
				+ " ON t.car_id = t1.id"
				+ " WHERE user_id = ?";

		//購入データより車種情報を全て取得するSELECT文を実行して取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1,id);
		ResultSet rs = pStmt.executeQuery();

		//BuyBeans型のarrylistを生成
		ArrayList<BuyBeans> buyHistoryList = new ArrayList<BuyBeans>();

		//(rsに)格納されたリスト分繰り返す
		//次のリストがなかったら取得した値をbuyHistoryListにadd()する
		while (rs.next()) {
			BuyBeans bb = new BuyBeans();
			bb.setId(rs.getInt("id"));
			bb.setDate(rs.getTimestamp("date"));
			bb.setDeliveryPrice(rs.getInt("delivery_price"));
			bb.setTotalPrice(rs.getInt("total_price"));
			bb.setColor(rs.getString("color"));
			bb.setPrice(rs.getInt("price"));
			bb.setCarName(rs.getString("car_name"));
			bb.setFileName(rs.getString("file_name"));
			bb.setDetail(rs.getString("detail"));
			buyHistoryList.add(bb);

		}
			return buyHistoryList;

		} catch (SQLException e) {
		        	e.printStackTrace();
		        	return null;
		} finally {
		 // データベース切断
				if (conn != null) {
		          try {
		                conn.close();
		           } catch (SQLException e) {
		                e.printStackTrace();
					}
				}
		}
	}

}
