package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBmanager;
import beans.CarBeans;

/**
 * carテーブル用のDAO
 * @author monju
 */

public class CarDao {

/**
 * 商品情報を指定ランダム数で取得する際のリスト
 * @param
 */

	public static ArrayList<CarBeans> getRandomCar(int limit) throws SQLException{
	    Connection conn = null;
        try {
        // データベースへ接続
		conn = DBmanager.getConnection();

		// TODO ここに処理を書いていく
		//SQL確認済み
		String sql = "SELECT * FROM car ORDER BY RAND() LIMIT ?";

		//limit個分の商品情報を取得するSELECT文を実行して取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1,limit);
		ResultSet rs = pStmt.executeQuery();

		//CarBeans型のarrylistを生成
		ArrayList<CarBeans> carList = new ArrayList<CarBeans>();

		//(rsに)格納されたリスト分繰り返す
		//次のリストがなかったら取得した値をcarListにadd()する
		while (rs.next()) {
			CarBeans car = new CarBeans();
			car.setId(rs.getInt("id"));
			car.setCarName(rs.getString("car_name"));
			car.setDetail(rs.getString("detail"));
			car.setFileName(rs.getString("file_name"));
			car.setPrice(rs.getInt("price"));
			carList.add(car);

		}
			return carList;

		} catch (SQLException e) {
		        	e.printStackTrace();
		        	return null;
		} finally {
		 // データベース切断
				if (conn != null) {
		          try {
		                conn.close();
		           } catch (SQLException e) {
		                e.printStackTrace();
					}
				}
		}
	}

	/**
	 * メーカーごとに絞り込んだリストを取得するメソッド
	 * @param
	 */

		public static ArrayList<CarBeans> getMakerCar(String id) throws SQLException{
		    Connection conn = null;
	        try {
	        // データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT * FROM car WHERE maker_id =?";

			//idに紐づくメーカーの車種情報を全て取得するSELECT文を実行して取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<CarBeans> makerList = new ArrayList<CarBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をmakerListにadd()する
			while (rs.next()) {
				CarBeans car = new CarBeans();
				car.setId(rs.getInt("id"));
				car.setMakerId(rs.getInt("maker_id"));
				car.setCarName(rs.getString("car_name"));
				car.setDetail(rs.getString("detail"));
				car.setFileName(rs.getString("file_name"));
				car.setPrice(rs.getInt("price"));
				makerList.add(car);

			}
				return makerList;

			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
					if (conn != null) {
			          try {
			                conn.close();
			           } catch (SQLException e) {
			                e.printStackTrace();
						}
					}
			}
		}

/**
 * 排気量別に車情報を取得するメソッド
 * @param lower
 * @param upper
 * @return
 * @throws SQLException
 */

		public static ArrayList<CarBeans> getDisplacement(String lower,String upper) throws SQLException{
		    Connection conn = null;
	        try {
	        // データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT * FROM car WHERE displacement >= ? AND displacement <= ?";

			//排気量別に車種情報を全て取得するSELECT文を実行して取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,lower);
			pStmt.setString(2,upper);
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<CarBeans> displacementList = new ArrayList<CarBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をmakerListにadd()する
			while (rs.next()) {
				CarBeans car = new CarBeans();
				car.setId(rs.getInt("id"));
				car.setMakerId(rs.getInt("maker_id"));
				car.setCarName(rs.getString("car_name"));
				car.setDetail(rs.getString("detail"));
				car.setFileName(rs.getString("file_name"));
				car.setPrice(rs.getInt("price"));
				displacementList.add(car);

			}
				return displacementList;

			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
					if (conn != null) {
			          try {
			                conn.close();
			           } catch (SQLException e) {
			                e.printStackTrace();
						}
					}
			}
		}

/**
 * 検索ボックスの値から車情報を取得する用
 * @param serchWord
 * @return
 * @throws SQLException
 */
		public static ArrayList<CarBeans> getSerchCarList(String serchWord) throws SQLException{
		    Connection conn = null;
	        try {
	        // データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT * FROM car WHERE car_name LIKE ? OR detail LIKE ?";

			//検索ワードに該当する車種情報を全て取得するSELECT文を実行して取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,"%" + serchWord +"%");
			pStmt.setString(2,"%" + serchWord +"%");
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<CarBeans> displacementList = new ArrayList<CarBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をmakerListにadd()する
			while (rs.next()) {
				CarBeans car = new CarBeans();
				car.setId(rs.getInt("id"));
				car.setMakerId(rs.getInt("maker_id"));
				car.setCarName(rs.getString("car_name"));
				car.setDetail(rs.getString("detail"));
				car.setFileName(rs.getString("file_name"));
				car.setPrice(rs.getInt("price"));
				displacementList.add(car);

			}
			return displacementList;

			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
					if (conn != null) {
			          try {
			                conn.close();
			           } catch (SQLException e) {
			                e.printStackTrace();
						}
					}
			}
		}

/**
 * 車の詳細情報を取得する用
 * @param ID
 * @return
 * @throws SQLException
 */
		public static CarBeans getCarDetailInfo(String ID) throws SQLException{
			CarBeans cb = new CarBeans();
		    Connection conn = null;
		        try {
		            // データベースへ接続
					conn = DBmanager.getConnection();

					// TODO ここに処理を書いていく
					//SQL確認済み
					String sql ="SELECT * FROM car WHERE id =?";
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1,ID);
					ResultSet rs = pStmt.executeQuery();

					while (rs.next()) {
						cb.setId(rs.getInt("id"));
						cb.setCarName(rs.getString("car_name"));
						cb.setCapa(rs.getInt("capa"));
						cb.setDetail(rs.getString("detail"));
						cb.setDisplacement(rs.getInt("displacement"));
						cb.setDriveSystem(rs.getString("drive_system"));
						cb.setFileName(rs.getString("file_name"));
						cb.setFuelEconomy(rs.getDouble("fuel_economy"));
						cb.setMakerId(rs.getInt("maker_id"));
						cb.setPower(rs.getString("power"));
						cb.setPrice(rs.getInt("price"));
						cb.setSize(rs.getString("size"));
						cb.setTorque(rs.getString("torque"));
						cb.setUrl(rs.getString("url"));

					}

				} catch (SQLException e) {
				        	e.printStackTrace();
				        	return null;
				} finally {
				 // データベース切断
						if (conn != null) {
				          try {
				                conn.close();
				           } catch (SQLException e) {
				                e.printStackTrace();
							}
						}
				}
		        return cb;
		}

/**
 * 詳細ページで閲覧してる車に近い車種情報を取得するメソッド
 * @param ID
 * @param price
 * @param capa
 * @return
 * @throws SQLException
 */
		public static CarBeans getCarCompareInfo(String ID,String price,String capa) throws SQLException{
			CarBeans cc = new CarBeans();
		    Connection conn = null;
		        try {
		            // データベースへ接続
					conn = DBmanager.getConnection();

					// TODO ここに処理を書いていく
					//SQL確認済み
					String sql ="SELECT * FROM car WHERE id!=? AND capa=? ORDER BY abs(price-?) ASC LIMIT 1";
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1,ID);
					pStmt.setString(2,capa);
					pStmt.setString(3,price);
					ResultSet rs = pStmt.executeQuery();

					while (rs.next()) {
						cc.setId(rs.getInt("id"));
						cc.setCarName(rs.getString("car_name"));
						cc.setCapa(rs.getInt("capa"));
						cc.setDetail(rs.getString("detail"));
						cc.setDisplacement(rs.getInt("displacement"));
						cc.setDriveSystem(rs.getString("drive_system"));
						cc.setFileName(rs.getString("file_name"));
						cc.setFuelEconomy(rs.getDouble("fuel_economy"));
						cc.setMakerId(rs.getInt("maker_id"));
						cc.setPower(rs.getString("power"));
						cc.setPrice(rs.getInt("price"));
						cc.setSize(rs.getString("size"));
						cc.setTorque(rs.getString("torque"));
						cc.setUrl(rs.getString("url"));

					}

				} catch (SQLException e) {
				        	e.printStackTrace();
				        	return null;
				} finally {
				 // データベース切断
						if (conn != null) {
				          try {
				                conn.close();
				           } catch (SQLException e) {
				                e.printStackTrace();
							}
						}
				}
		        return cc;
		}

/**
 * 管理者画面から車種名・メーカー・詳細情報で車種情報を取得するメソッド
 * @param carName
 * @param maker
 * @param keyWord
 * @return
 * @throws SQLException
 */
		public static ArrayList<CarBeans> getMasterSerchCarList(String carName,String maker,String keyWord) throws SQLException{
		    Connection conn = null;
	        try {
	        // データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			//SQL確認済み
			String sql = "SELECT t.* FROM car t INNER JOIN maker t1 ON t.maker_id=t1.id WHERE";

			if(!maker.equals("ALL")) {
				sql += " t1.maker_name = '" + maker + "'";
			}else{
				sql += " t1.maker_name != ''";
			}
			if(!carName.equals("")) {
				sql += " AND t.car_name LIKE '%" + carName + "%'";
			}
			if(!keyWord.equals("")) {
				sql += " AND t.detail LIKE '%" + keyWord + "%'";
			}

			//検索ワードに該当する車種情報を全て取得するSELECT文を実行して取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			//CarBeans型のarrylistを生成
			ArrayList<CarBeans> masterCarList = new ArrayList<CarBeans>();

			//(rsに)格納されたリスト分繰り返す
			//次のリストがなかったら取得した値をmasterCarListにadd()する
			while (rs.next()) {
				CarBeans car = new CarBeans();
				car.setId(rs.getInt("id"));
				car.setMakerId(rs.getInt("maker_id"));
				car.setCarName(rs.getString("car_name"));
				car.setDetail(rs.getString("detail"));
				car.setFileName(rs.getString("file_name"));
				car.setPrice(rs.getInt("price"));

				masterCarList.add(car);
			}
				return masterCarList;

			} catch (SQLException e) {
			        	e.printStackTrace();
			        	return null;
			} finally {
			 // データベース切断
					if (conn != null) {
			          try {
			                conn.close();
			           } catch (SQLException e) {
			                e.printStackTrace();
						}
					}
			}
		}

	public CarBeans updateCarInfo(String carName,String size,int capa,int displacement,String power,
			String torque,double fuelEconomy,String driveSystem,int price,String url,String name,String detail,int id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE car SET car_name=?,size=?,capa=?,displacement=?,power=?,torque=?,fuel_economy=?,drive_system=?,price=?,url=?,file_name=?,detail=? WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, carName);
			pStmt.setString(2, size);
			pStmt.setInt(3, capa);
			pStmt.setInt(4, displacement);
			pStmt.setString(5, power);
			pStmt.setString(6, torque);
			pStmt.setDouble(7, fuelEconomy);
			pStmt.setString(8, driveSystem);
			pStmt.setInt(9, price);
			pStmt.setString(10, url);
			pStmt.setString(11, name);
			pStmt.setString(12, detail);
			pStmt.setInt(13, id);
			pStmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

}

