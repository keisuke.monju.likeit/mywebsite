package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 購入データ
 * @author k-monju
 *
 */

public class BuyBeans implements Serializable{
	private int id;
	private int userId;
	private int carId;
	private Date date;
	private int deliveryPrice;
	private int totalPrice;
	private String color;

	private String carName;
	private int price;
	private String fileName;
	private String detail;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCarId() {
		return carId;
	}
	public void setCarId(int carId) {
		this.carId = carId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getBuyDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日k時mm分ss秒");
        return sdf.format(date);
	}

	public int getDeliveryPrice() {
		return deliveryPrice;
	}
	public void setDeliveryPrice(int deliveryPrice) {
		this.deliveryPrice = deliveryPrice;
	}
	public String getFormatDeliveryPrice() {
		return String.format("%,d", this.deliveryPrice);
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}



	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}


}
