package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * ユーザーデータ
 * @author k-monju
 *
 */

public class UserBeans implements Serializable{
	private int id;
	private String loginId;
	private String userName;
	private Date birthDate;
	private String address;
	private int cardNo;
	private int cardlLimitMM;
	private int cardlLimitYY;
	private String makerId;

	// ログインセッションを保存するためのコンストラクタ
	public UserBeans(String loginId, String userName) {
		this.loginId = loginId;
		this.userName = userName;
	}

	//新規登録時のログインIDが存在するか確認用
	public UserBeans(String loginId) {
		this.loginId = loginId;

	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCardNo() {
		return cardNo;
	}
	public void setCardNo(int cardNo) {
		this.cardNo = cardNo;
	}
	public int getCardlLmitMM() {
		return cardlLimitMM;
	}
	public void setCardlLimitMM(int cardlLimitMM) {
		this.cardlLimitMM = cardlLimitMM;
	}
	public int getCardlLimitYY() {
		return cardlLimitYY;
	}
	public void setCardlLimitYY(int cardlLimitYY) {
		this.cardlLimitYY = cardlLimitYY;
	}
	public String getMakerId() {
		return makerId;
	}
	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}


}
