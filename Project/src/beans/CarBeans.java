package beans;

import java.io.Serializable;

/**
 * 車種データ
 * @author k-monju
 *
 */

public class CarBeans implements Serializable{
	private int id;
	private String carName;
	private int makerId;
	private String size;
	private int capa;
	private int displacement;
	private String power;
	private String torque;
	private Double fuelEconomy;
	private String driveSystem;
	private int price;
	private String fileName;
	private String detail;
	private String url;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public int getMakerId() {
		return makerId;
	}
	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getCapa() {
		return capa;
	}
	public void setCapa(int capa) {
		this.capa = capa;
	}
	public int getDisplacement() {
		return displacement;
	}
	public void setDisplacement(int displacement) {
		this.displacement = displacement;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getTorque() {
		return torque;
	}
	public void setTorque(String torque) {
		this.torque = torque;
	}
	public Double getFuelEconomy() {
		return fuelEconomy;
	}
	public void setFuelEconomy(Double fuelEconomy) {
		this.fuelEconomy = fuelEconomy;
	}
	public String getDriveSystem() {
		return driveSystem;
	}
	public void setDriveSystem(String driveSystem) {
		this.driveSystem = driveSystem;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}



}
