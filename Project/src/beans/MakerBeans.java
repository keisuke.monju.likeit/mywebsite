package beans;

import java.io.Serializable;

/**
 * メーカーデータ
 * @author k-monju
 *
 */

public class MakerBeans implements Serializable{
	private int id;
	private String makerName;
	private String country;



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}


}
