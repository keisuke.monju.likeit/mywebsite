package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class MyUtil {
	/**任意項目の入力値が空文字の場合、nullを返すメソッド*/
	public static String checkParam(String param) {
		if(param.equals("")) {
			return null;
		}
			return param;
	}

	/**入力されたpasswordをMD5で暗号化するメソッド*/
	public static String passcode(String password) {
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		//例外処理
		try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			return result;

		}catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
		}

			return null;
	}

	/**配送料を決めるメソッド*/
	public static int getDeliveryPrice(String delivery) {
		if(delivery.equals("shop")) {
			int deliveryPrice = 0;
			return deliveryPrice;
		}{
			int deliveryPrice = 50000;
			return deliveryPrice;
		}

	}

}
