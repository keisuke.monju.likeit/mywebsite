package carec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import beans.MakerBeans;
import dao.CarDao;
import dao.MakerDao;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetailServlet")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//リクエストパラメーターのidを取得
		String ID = request.getParameter("id");
		try {
		//idに紐づく車のすべての情報を取得するメソッド
			CarBeans cb = CarDao.getCarDetailInfo(ID);
			request.setAttribute("cb", cb);

		//mekar_idに紐づくメーカー名を取得するメソッド
			int id = cb.getMakerId();
			MakerBeans mb =MakerDao.getMakerName(id);
			request.setAttribute("mb", mb);

		//国産メーカー一覧を表示するメソッド
		ArrayList<MakerBeans> jpnMakerList = MakerDao.findJpnMaker();
		request.setAttribute("jpnMakerList", jpnMakerList);

		//輸入メーカー一覧を表示するメソッド
		ArrayList<MakerBeans> outMakerList = MakerDao.findOutMaker();
		request.setAttribute("outMakerList", outMakerList);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
