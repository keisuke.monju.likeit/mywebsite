package carec;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import beans.MakerBeans;
import dao.CarDao;
import dao.MakerDao;

/**
 * Servlet implementation class ItemCompareServlet
 */
@WebServlet("/ItemCompareServlet")
public class ItemCompareServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemCompareServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ID = request.getParameter("id");
		String capa = request.getParameter("capa");
		String price = request.getParameter("price");

		try {
			//idに紐づく車のすべての情報を取得するメソッド
			CarBeans cb = CarDao.getCarDetailInfo(ID);
			request.setAttribute("cb", cb);

			//mekar_idに紐づくメーカー名を取得するメソッド
			int id = cb.getMakerId();
			MakerBeans mb =MakerDao.getMakerName(id);
			request.setAttribute("mb", mb);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			//idの車種に似た車のすべての情報を取得するメソッド
			CarBeans cc = CarDao.getCarCompareInfo(ID, price, capa);
			request.setAttribute("cc", cc);

			//mekar_idに紐づくメーカー名を取得するメソッド
			int id = cc.getMakerId();
			MakerBeans ccmb =MakerDao.getMakerName(id);
			request.setAttribute("ccmb", ccmb);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemCompare.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
