package carec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;
import util.MyUtil;

/**
 * Servlet implementation class UserNewRegistServlet
 */
@WebServlet("/UserNewRegistServlet")
public class UserNewRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewRegistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserNewRegist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		// 新規登録画面での入力内容を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordre = request.getParameter("passwordre");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String address = request.getParameter("address");
		//checkParam()は空文字の際に、nullを返すメソッド
		String cardNo = MyUtil.checkParam(request.getParameter("cardNo"));
		String cardLimitMM = MyUtil.checkParam(request.getParameter("cardLimitMM"));
		String cardLimitYY = MyUtil.checkParam(request.getParameter("cardLimitYY"));
		String cardName = request.getParameter("cardName");
		String maker = request.getParameter("maker");

		//UserDaoインスタンス生成
		UserDao userDao = new UserDao();

		//loginIdが登録済みかチェック
		UserBeans ub = userDao.idCheck(loginId);

		//入力されたパスワードと確認用が一致しない場合
		//エラーメッセージを表示して、新規登録画面を再度表示
		//パスワード以外の入力された項目は内容を保持する

		//TODO makerのみ値の保持が不明のため後で実装
		if (!password.equals(passwordre)) {
			request.setAttribute("errMsg", "パスワードが確認と一致しません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birthday);
			request.setAttribute("address", address);
			request.setAttribute("cardNo", cardNo);
			request.setAttribute("cardLimitMM", cardLimitMM);
			request.setAttribute("cardLimitYY", cardLimitYY);
			request.setAttribute("cardName", cardName);
			request.setAttribute("maker", maker);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserNewRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力されたログインIDが既に登録済みの場合
		//エラーメッセージを表示して、新規登録画面を再度表示
		//パスワード以外の入力された項目は内容を保持する

		//TODO makerのみ値の保持が不明のため後で実装
		if (ub != null) {
			request.setAttribute("errMsg", "ログインIDは既に使用済みです");
			request.setAttribute("loginID", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthday", birthday);
			request.setAttribute("address", address);
			request.setAttribute("cardNo", cardNo);
			request.setAttribute("cardLimitMM", cardLimitMM);
			request.setAttribute("cardLimitYY", cardLimitYY);
			request.setAttribute("cardName", cardName);
			request.setAttribute("maker", maker);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserNewRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		userDao.useNewReg(loginId, passwordre, cardName, birthday, address, cardNo, cardLimitMM, cardLimitYY, maker);
		response.sendRedirect("LoginServlet");
	}

}
