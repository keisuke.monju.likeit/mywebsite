package carec;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.CarBeans;
import beans.MakerBeans;
import dao.CarDao;
import dao.MakerDao;

/**
 * Servlet implementation class MasterItemUpdateServlet
 */
@WebServlet("/MasterItemUpdateServlet")
@MultipartConfig(location="C:\\Users\\radwi\\Documents\\mywebsite\\Project\\WebContent\\imag", maxFileSize=1048576)
public class MasterItemUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MasterItemUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session =request.getSession();
		String ID = request.getParameter("id");
		session.setAttribute("ID", ID);

		try {
			CarBeans cb = CarDao.getCarDetailInfo(ID);
			request.setAttribute("cb", cb);

			int id = cb.getMakerId();
			MakerBeans mb = MakerDao.getMakerName(id);
			request.setAttribute("mb", mb);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/MasterItemUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session =request.getSession();

		CarDao cd = new CarDao();

		String ID = (String) session.getAttribute("ID");
		int id = Integer.parseInt(ID);
		String carName = request.getParameter("carName");
		String size = request.getParameter("size");
		int capa = Integer.parseInt(request.getParameter("capa"));
		int displacement = Integer.parseInt(request.getParameter("displacement"));
		String power = request.getParameter("power");
		String torque = request.getParameter("torque");
		double fuelEconomy = Double.parseDouble(request.getParameter("fuelEconomy"));
		String driveSystem = request.getParameter("driveSystem");
		int price = Integer.parseInt(request.getParameter("price"));
		String url = request.getParameter("url");
		//String fileName = request.getParameter("fileName");
		String detail = request.getParameter("detail");

        Part part = request.getPart("fileName");
        String name = this.getFileName(part);
        part.write(name);


		cd.updateCarInfo(carName, size, capa, displacement, power, torque, fuelEconomy, driveSystem, price, url, name, detail,id);
		session.removeAttribute("ID");

		response.sendRedirect("MasterItemEditServlet");
	}

        private String getFileName(Part part) {
            String name = null;
            for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
                if (dispotion.trim().startsWith("filename")) {
                    name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                    name = name.substring(name.lastIndexOf("\\") + 1);
                    break;
                }
            }
            return name;
        }

}
