package carec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import dao.BuyDao;
import dao.UserDao;

/**
 * Servlet implementation class BuyListServlet
 */
@WebServlet("/BuyListServlet")
public class BuyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String carName = (String)session.getAttribute("cn");
		request.setAttribute("cn",carName);

		//セッションスコープよりlogin_idを取得
		String loginId = (String)session.getAttribute("loginId");
		//login_idよりidを取得するメソッド
			try {
				int id = UserDao.getId(loginId);
		//取得したidより購入情報を取得
				ArrayList<BuyBeans> buyHistoryList = BuyDao.getBuyHistory(id);
				request.setAttribute("buyHistoryList", buyHistoryList);
			} catch (SQLException e) {
				e.printStackTrace();
			}

		//車種名が取得できた場合のみ購入完了メッセージを表示する
		if(carName !=null){
			request.setAttribute("buyMsg","の購入が完了しました" );
		}

		//セッションに詰めた車種名を削除
		session.removeAttribute("cn");

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/BuyList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
