package carec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import beans.MakerBeans;
import dao.CarDao;
import dao.MakerDao;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet("/TopServlet")
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			//トップページにランダムで四点表示するメソッド
			ArrayList<CarBeans> carList = CarDao.getRandomCar(4);
			request.setAttribute("carList", carList);

			//国産メーカー一覧を表示するメソッド
			ArrayList<MakerBeans> jpnMakerList = MakerDao.findJpnMaker();
			request.setAttribute("jpnMakerList", jpnMakerList);

			//輸入メーカー一覧を表示するメソッド
			ArrayList<MakerBeans> outMakerList = MakerDao.findOutMaker();
			request.setAttribute("outMakerList", outMakerList);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/Top.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
