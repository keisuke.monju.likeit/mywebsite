package carec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/Login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		// ログイン時の入力内容を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		//入力内容を引数に渡して、findByLoginInfoメソッドを実行
		UserDao userDao = new UserDao();
		UserBeans userBeans = userDao.findByLoginInfo(loginId,password);

		/**テーブルに該当のデータがなかった場合*/
		if(userBeans==null) {
			//エラーメッセージをセットして、Login.jspにフォワード
			request.setAttribute("errMsg","ログインIDまたはパスワードが違います" );
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/**テーブルに該当のデータが存在した場合*/
		//ユーザーidをセッションにセットし、トップページのサーブレットにリダイレクト
		HttpSession session = request.getSession();
		session.setAttribute("loginId", loginId);

		response.sendRedirect("TopServlet");
	}

}
