package carec;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CarBeans;
import dao.BuyDao;
import dao.CarDao;
import dao.UserDao;
import util.MyUtil;

/**
 * Servlet implementation class ItemBuyServlet
 */
@WebServlet("/ItemBuyServlet")
public class ItemBuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemBuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		try {
			CarBeans cb = CarDao.getCarDetailInfo(id);
			request.setAttribute("cb", cb);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemBuy.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String carName = request.getParameter("carName");
		session.setAttribute("cn",carName);

		//セレクトボックスの選択内容を取得
		//取得した内容により配送料金を決めるメソッド
		int deliveryPrice = MyUtil.getDeliveryPrice(request.getParameter("delivery"));

		int totalPrice = Integer.parseInt(request.getParameter("price")) + deliveryPrice;

		String color = request.getParameter("color");
		//リクエストスコープよりcar_idを取得
		String carId = request.getParameter("id");

		//セッションスコープよりlogin_idを取得
		String loginId = (String)session.getAttribute("loginId");
		try {
			//login_idよりidを取得するメソッド
			int userId = UserDao.getId(loginId);
			BuyDao.newBuyDate(userId, carId, deliveryPrice, totalPrice, color);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect("BuyListServlet");
	}

}
