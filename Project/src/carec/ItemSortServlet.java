package carec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import beans.MakerBeans;
import dao.CarDao;
import dao.MakerDao;

/**
 * Servlet implementation class ItemSortServlet
 */
@WebServlet("/ItemSortServlet")
public class ItemSortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemSortServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//				try {
		//					HttpSession session = request.getSession();
		//					//メーカーで絞り込んで表示するメソッド
		//					maker = session.getAttribute("maker_id");
		//					ArrayList<CarBeans>	makerList = CarDao.getMakerCar(maker);
		//					request.setAttribute("makerList", makerList);
		//
		//				} catch (SQLException e) {
		//					// TODO 自動生成された catch ブロック
		//					e.printStackTrace();
		//				}
		// リクエストパラメータよりidを取得
		String id = request.getParameter("id");
		//排気量別のリクエストパラメーターを取得
		String lower = request.getParameter("lower");
		String upper = request.getParameter("upper");

		try {
			//取得した排気量で排気量別の車一覧を取得するメソッド
			ArrayList<CarBeans> displacementList = CarDao.getDisplacement(lower, upper);
			request.setAttribute("displacementList", displacementList);

			// 取得したidで車一覧を取得するメソッド（パラメータで絞り込む）
			ArrayList<CarBeans> makerList = CarDao.getMakerCar(id);
			// 次画面に取得した一覧を送る
			request.setAttribute("makerList", makerList);
			//idがnullで無かった時、該当のメーカー名を取得するif文
			if(id != null) {
				MakerBeans mb = MakerDao.getMakerName(Integer.parseInt(id));
				request.setAttribute("mb", mb);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemSort.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//検索ワードに該当する車種がない場合、ItemSort.jsp遷移してエラーメッセージを返す
		String searchWord = request.getParameter("search_word");
		try {
			ArrayList<CarBeans> serchCarList = CarDao.getSerchCarList(searchWord);
			request.setAttribute("serchCarList", serchCarList);

			if(serchCarList==null) {

				//エラーメッセージをセットして、直前のページにエラー文を表示する
				request.setAttribute("serchErrMsg","※検索ワードに該当する車種は存在しません※");

				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemSort.jsp");
				dispatcher.forward(request, response);
				return;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ItemSort.jsp");
		dispatcher.forward(request, response);
	}

}
