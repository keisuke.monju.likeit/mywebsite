<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>基本情報更新ページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="TopServlet" method="post">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Search">
    </form>
  </div>
</nav>


<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-5">基本情報修正</h1>
  <p class="lead alert alert-danger" role="alert">※エラー文表示箇所</p>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
	     <div><a href="UserUpdateServlet"><button type="button" class="btn btn-primary btn-block">登録内容の変更</button></a></div>
	     <div><a href="BuyListServlet"><button type="button" class="btn btn-primary btn-block">購入履歴</button></a></div>
	     <div><a href="MasterItemEditServlet"><button type="button" class="btn btn-primary btn-block">商品編集※管理者のみ</button></a></div>
    </div>

    <div class="border col-7 container">
                <form action="UserUpdateServlet" method="post">
        <br>
        <h2>情報更新</h2>
        <br>
        <div class="row">
            <div class="col-md">
                    <div class="form-group">
                        <label>※ログインID：</label>
                        <input type="text" class="form-control" value="登録時のIDを初期値" disabled>
                    </div>
                    <div class="form-group">
                        <label>ユーザ名：</label>
                        <input type="text" class="form-control" placeholder="初期値を入れる">
                    </div>
                    <div class="form-group">
                        <label>パスワード：</label>
                        <input type="password" class="form-control" placeholder="※6文字以上で半角英数字を必ず使用すること">
                    </div>
                    <div class="form-group">
                        <label>パスワード(確認)：</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>生年月日：</label>
                        <input type="date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>住所：</label>
                        <input type="text" class="form-control" placeholder="神奈川県〇〇市〇〇区〇〇〇町X-X-X □ビル1 XXX号室">
                    </div>
                    <div class="form-group">
                        <label>好きなメーカー：</label>
                        <select class="form-control">
                            <option>トヨタ</option>
                            <option>ホンダ</option>
                            <option>日産</option>
                            <option>三菱</option>
                            <option>スバル</option>
                            <option>BMW</option>
                            <option>ベンツ</option>
                            <option>アウディ</option>
                            <option>MINI</option>
                            <option>Jeep</option>
                            <option>ポルシェ</option>
                            <option>ランボルギーニ</option>
                        </select>
                    </div>

					<div class="panel-heading form-group">
                        <label>カード情報：</label>
					      <div class="row ">
					              <div class="col-md-12">
					                  <input type="text" class="form-control" placeholder="口座番号">
					              </div>
					          </div>
					     <div class="row ">
					              <div class="col-md-3 col-sm-3 col-xs-3">
					                  <span class="help-block text-muted small-font"> Expiry Month</span>
					                  <input type="text" class="form-control" placeholder="月">
					              </div>
					         <div class="col-md-3 col-sm-3 col-xs-3">
					                  <span class="help-block text-muted small-font">  Expiry Year</span>
					                  <input type="text" class="form-control" placeholder="年">
					              </div>
					        <div class="col-md-3 col-sm-3 col-xs-3">
					                  <span class="help-block text-muted small-font">  CCV</span>
					                  <input type="text" class="form-control" placeholder="セキュリティコード">
					              </div>
					          </div>
					     <div class="row ">
					              <div class="col-md-12 pad-adjust">
					                  <span class="help-block text-muted small-font"> Name</span>
					                  <input type="text" class="form-control" placeholder="口座名義">
					              </div>
					          </div>
					</div>

            </div>
        </div>
        <div class="row center-block text-center">
            <div class="col-1">
            </div>
            <div class="col-5">
                <button type="button" onclick="history.back()" class="btn btn-outline-secondary btn-block">戻る</button>
            </div>
            <div class="col-5">
                <button type="button" class="btn btn-outline-primary btn-block">更新</button>
            </div>
        </div>
                </form>
    </div>
</div>
</body>

</html>