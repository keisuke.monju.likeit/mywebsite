<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>商品詳細ページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="ItemSortServlet" method="post">
      <input type="text" name="search_word" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <a><button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Search</button></a>
    </form>
  </div>
</nav>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
	<h1 class="display-5">${cb.carName}</h1>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
		●国産車：
		<c:forEach var="maker" items="${jpnMakerList}">
		     <div><a href="ItemSortServlet?id=${maker.id }"><button type="button" class="btn btn-info btn-block">${maker.makerName}</button></a></div>
		</c:forEach>
	    ●輸入車：
	    <c:forEach var="maker" items="${outMakerList}">
		     <div><a href="ItemSortServlet?id=${maker.id }"><button type="button" class="btn btn-info btn-block">${maker.makerName}</button></a></div>
		</c:forEach>
	    ●排気量：
		     <div><a href="ItemSortServlet?upper=999&lower=0"><button type="button" class="btn btn-info btn-block">999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=1999&lower=1000"><button type="button" class="btn btn-info btn-block">1,000cc～1,999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=2999&lower=2000"><button type="button" class="btn btn-info btn-block">2,000cc～2,999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=5999&lower=3000"><button type="button" class="btn btn-info btn-block">3,000cc以上</button></a></div>
    </div>

	<div class="container row featurette mx-auto text-center">
	<div class="col-sm-4 py-5">
		<img src="imag/${cb.fileName}" alt="BMW" class="img-thumbnail" width="250" height="250">
			<p>${cb.detail}</p>
	</div>
    <div class="col-sm-8">
			      <div>
			        <p class="lead">詳細情報表示</p>
						<table class="table table-bordered">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">項目名</th>
						      <th scope="col">ステータス</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">1</th>
						      <td>メーカー</td>
						      <td>${mb.makerName}</td>
						    </tr>
						    <tr>
						    <tr>
						      <th scope="row">2</th>
						      <td>全長x全幅x全高（mm）</td>
						      <td>${cb.size}</td>
						    </tr>
						    <tr>
						      <th scope="row">3</th>
						      <td>定員（名）</td>
						      <td>${cb.capa}</td>
						    </tr>
						    <tr>
						      <th scope="row">4</th>
						      <td>総排気量（cc）</td>
						      <td>${cb.displacement}</td>
						    </tr>
						    <tr>
						      <th scope="row">5</th>
						      <td>最高出力（ps（kw）/rpm）</td>
						      <td>${cb.power}</td>
						    </tr>
						    <tr>
						      <th scope="row">6</th>
						      <td>最大トルク（kgm（Nm）/rpm）</td>
						      <td>${cb.torque}</td>
						    </tr>
						    <tr>
						      <th scope="row">7</th>
						      <td>燃料消費率JC08モード（国土交通省審査値）（km/ℓ）</td>
						      <td>${cb.fuelEconomy}</td>
						    </tr>
						    <tr>
						      <th scope="row">8</th>
						      <td>駆動方式</td>
						      <td>${cb.driveSystem}</td>
						    </tr>
						    <tr>
						      <th scope="row">9</th>
						      <td>料金</td>
						      <td>${cb.price}</td>
						    </tr>
						  </tbody>
						</table>
			      </div>
			</div>

			<div class="container py-5">
				<div class="btn-toolbar mx-auto text-center" role="toolbar" aria-label="Toolbar with button groups">
					<div class = "container">
					  <div class="btn-group mr-2" role="group" aria-label="First group">
					    <a href="${cb.url}" target="_blank"> <button type="button" class="btn btn-primary">更に詳細を見る</button></a>
					  </div>
					  <div class="btn-group mr-2" role="group" aria-label="Second group">
					  <form action="ItemCompareServlet" method="get">
						<input type="hidden" name="price" value="${cb.price}">
						<input type="hidden" name="id" value="${cb.id}">
						<input type="hidden" name="capa" value="${cb.capa}">
					    <a><button type="submit" class="btn btn-info">似た車との比較</button></a>
					  </form>
					  </div>
					  <div class="btn-group" role="group" aria-label="Third group">
					    <a href="ItemBuyServlet?id=${cb.id}"><button type="button" class="btn btn-danger">この車を購入する</button></a>
					  </div>
					 </div>
				</div>
			</div>
            <div class="col-12 py-3">
                <button type="button" onclick="history.back()" class="btn btn-outline-secondary btn-block">戻る</button>
            </div>
	</div>

</div>

</body>
</html>