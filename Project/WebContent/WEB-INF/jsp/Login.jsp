<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>ログインページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="ItemSortServlet" method="post">
      <input type="text" name="search_word" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <a><button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Search</button></a>
    </form>
  </div>
</nav>

<div align="center"><h1>ログイン画面</h1></div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<div class = "container">
	<form action="LoginServlet" method="post">
  <div class="form-group">
    	<label for="UserID">User ID</label>
    	<input required type="text" class="form-control" id="UserID" placeholder="User ID" name="loginId">
  </div>
  <div class="form-group">
    	<label for="exampleInputPassword1">Password</label>
    	<input required type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
  </div>
  		<input type="submit" class="btn btn-primary btn-block" value="ログイン">
	</form>
		<a href="UserNewRegistServlet"><button type="button" class="btn btn-link">※ユーザIDをお持ちでない方はこちらを押してください</button></a>

</div>
</body>

</html>