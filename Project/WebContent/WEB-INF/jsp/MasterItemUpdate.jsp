<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>商品更新ページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="TopServlet" method="post">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Search">
    </form>
  </div>
</nav>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-5">商品更新</h1>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
	     <div><a href="MasterItemEditServlet"><button type="button" class="btn btn-primary btn-block">商品編集</button></a></div>
	     <div><a href="MasterItemNewRegistServlet"><button type="button" class="btn btn-primary btn-block">商品登録</button></a></div>
	     <div><a href="UserInfoServlet"><button type="button" class="btn btn-primary btn-block">Myページ</button></a></div>
    </div>



    <div class="border col-7 container">
		<form action="MasterItemUpdateServlet" method="post" enctype="multipart/form-data">
	        <br>
	        <h2>商品情報更新</h2>
	        <br>
	        <div class="row">
	            <div class="col-md">
	                    <div class="form-group">
	                        <label>車種名：</label>
	                        <input type="text" class="form-control" name="carName" value="${cb.carName}">
	                    </div>
	                    <div class="form-group">
	                        <label>メーカー：</label>
	                        <input type="text" class="form-control" value="${mb.makerName}" disabled>
	                    </div>
	                    <div class="form-group">
	                        <label>全長x全幅x全高（mm）：</label>
	                        <input type="text" class="form-control" name="size" value="${cb.size}">
	                    </div>
	                    <div class="form-group">
	                        <label>定員（名）：</label>
	                        <input type="text" class="form-control" name="capa" value="${cb.capa}">
	                    </div>
	                    <div class="form-group">
	                        <label>総排気量（cc）：</label>
	                        <input type="text" class="form-control" name="displacement" value="${cb.displacement}">
	                    </div>
	                    <div class="form-group">
	                        <label>最高出力（ps（kw）/rpm）：</label>
	                        <input type="text" class="form-control" name="power" value="${cb.power}">
	                    </div>
	                    <div class="form-group">
	                        <label>最大トルク（kgm（Nm）/rpm）：</label>
	                        <input type="text" class="form-control" name="torque" value="${cb.torque}">
	                    </div>
	                    <div class="form-group">
	                        <label>燃料消費率JC08モード（km/ℓ）：</label>
	                        <input type="text" class="form-control" name="fuelEconomy" value="${cb.fuelEconomy}">
	                    </div>
	                    <div class="form-group">
	                        <label>駆動方式：</label>
	                        <input type="text" class="form-control" name="driveSystem" value="${cb.driveSystem}">
	                    </div>
	                    <div class="form-group">
	                        <label>料金：</label>
	                        <input type="text" class="form-control" name="price" value="${cb.price}">
	                    </div>
	                    <div class="form-group">
	                        <label>URL：</label>
	                        <input type="text" class="form-control" name="url" value="${cb.url}">
	                    </div>
	                    <div class="form-group">
	                        <label>商品画像：</label>
	                        <img src="imag/${cb.fileName}" class="img-thumbnail" width="150" height="150">
	                        <input type="file" class="form-control" name="fileName">
	                    </div>
						<div class="form-group">
						    <label for="exampleFormControlTextarea1">商品説明文</label>
						    <textarea  class="form-control" id="exampleFormControlTextarea1" rows="3" name="detail">${cb.detail}</textarea>
						</div>
	            </div>
	        </div>
	        <div class="row center-block text-center">
	            <div class="col-1">
	            </div>
	            <div class="col-5">
	                <button type="button"  onclick="history.back()" class="btn btn-outline-secondary btn-block">戻る</button>
	            </div>
	            <div class="col-5">
	                <input type="submit" class="btn btn-outline-primary btn-block" value="商品更新">
	            </div>
	        </div>
		</form>
    </div>
</div>

</body>

</html>