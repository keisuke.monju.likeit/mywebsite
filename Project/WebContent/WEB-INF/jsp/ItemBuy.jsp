<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>商品購入ページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="ItemSortServlet" method="post">
      <input type="text" name="search_word" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <a><button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Search</button></a>
    </form>
  </div>
</nav>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
	<h1 class="display-5">購入画面</h1>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
		●国産車：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">国内メーカー名</button></a></div>
	    ●輸入車：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">国外メーカー名</button></a></div>
	    ●排気量：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">990cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">1,000cc～1,900cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">2,000cc～2,900cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">3,000cc以上</button></a></div>
    </div>
<form action="ItemBuyServlet" method="post">
<input type="hidden" name="price" value="${cb.price}">
<input type="hidden" name="id" value="${cb.id}">
<input type="hidden" name="carName" value="${cb.carName}">

			<div class="container row featurette">
	<div class="col-sm-4 py-5">
			<img src="imag/${cb.fileName}" alt="BMW" class="img-thumbnail" width="500" height="500">
	</div>
    <div class="col-sm-8">


			      <div class="col-md-7 order-md-2">
			        <p class="lead">商品内容</p>
						<table class="table table-bordered">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">項目名</th>
						      <th scope="col">ステータス</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">1</th>
						      <td>車種名</td>
						      <td>${cb.carName}</td>
						    </tr>
						    <tr>
						      <th scope="row">2</th>
						      <td>料金</td>
						      <td>${cb.price}円</td>
						    </tr>
						    <tr>
						      <th scope="row">3</th>
		                        <td>配送方法</td>
		                        <td><select required class="form-control" name="delivery">
		                            <option value="shop">店頭受け取り</option>
		                            <option value="home">自宅受け取り</option>
		                        </select></td>
						    </tr>
						    <tr>
						      <th scope="row">4</th>
						      <td>合計金額</td>
						      <td>${cb.price}円</td>
						    </tr>
						    <tr>
						      <th scope="row">5</th>
						      <td>カラー</td>
						      <td><select required class="form-control" name="color">
		                            <option value="black">ブラック</option>
		                            <option value="white">ホワイト</option>
		                            <option value="red">レッド</option>
		                            <option value="blue">ブルー</option>
		                            <option value="yellow">イエロー</option>
		                        </select></td>
						    </tr>
						  </tbody>
						</table>
			      </div>
			</div>
			<div class="container py-5 w-25 p-3 btn-block">
				<input type="submit" class="btn btn-danger" value="購入する">
			</div>


	</div>
	</form>
</div>

</body>
</html>