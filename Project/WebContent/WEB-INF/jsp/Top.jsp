<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<title>車比較・購入サイト</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="ItemSortServlet" method="post">
      <input type="text" name="search_word" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <a><button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Search</button></a>
    </form>
  </div>
</nav>

<div>
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  </ol>
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <a href="ItemDetailServlet"><img src="imag/urusuTop.jpg" class="d-block w-100" alt="おススメ商品①"></a>
	    </div>
	    <div class="carousel-item">
	      <a href="ItemDetailServlet"><img src="imag/ダッチラムTop.jpg" class="d-block w-100" alt="おススメ商品②"></a>
	    </div>
	    <div class="carousel-item">
	      <a href="ItemDetailServlet"><img src="imag/Top3.jpg" class="d-block w-100" alt="おススメ商品③"></a>
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
		●国産車：
		<c:forEach var="maker" items="${jpnMakerList}">
		     <div><a href="ItemSortServlet?id=${maker.id }"><button type="button" class="btn btn-info btn-block">${maker.makerName}</button></a></div>
		</c:forEach>
	    ●輸入車：
	    <c:forEach var="maker" items="${outMakerList}">
		     <div><a href="ItemSortServlet?id=${maker.id }"><button type="button" class="btn btn-info btn-block">${maker.makerName}</button></a></div>
		</c:forEach>
	    ●排気量：
		     <div><a href="ItemSortServlet?upper=999&lower=0"><button type="button" class="btn btn-info btn-block">999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=1999&lower=1000"><button type="button" class="btn btn-info btn-block">1,000cc～1,999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=2999&lower=2000"><button type="button" class="btn btn-info btn-block">2,000cc～2,999cc以下</button></a></div>
		     <div><a href="ItemSortServlet?upper=5999&lower=3000"><button type="button" class="btn btn-info btn-block">3,000cc以上</button></a></div>
    </div>
    <div class="col-sm-10">
		<c:forEach var="car" items="${carList}">
			<div class="row featurette">
			      <div class="col-md-7 order-md-2">
			        <h2 class="featurette-heading">${car.carName}</h2>
			        <p class="lead">${car.detail}</p>
			        <p class="lead text-right">${car.price}円</p>
			      </div>
			      <div class="col-md-5 order-md-1 mx-auto text-center">
			        <a href="ItemDetailServlet?id=${car.id}"><img src="imag/${car.fileName}" alt="BMW" class="img-thumbnail"></a>
			      </div>
			</div>
		</c:forEach>
    </div>
</div>
</body>
</html>