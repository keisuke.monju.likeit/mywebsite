<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
	integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
	crossorigin="anonymous">

<title>商品管理ページ</title>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light"
		style="background-color: #e3f2fd;">
		<a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span
						class="sr-only">(current)</span></a></li>
				<li class="nav-item"><a class="nav-link"
					href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="UserInfoServlet"><i
						class="fas fa-user"></i>Myページ</a></li>
			</ul>
			<form class="form-inline my-2 my-lg-6" action="TopServlet"
				method="post">
				<input class="form-control mr-sm-2" type="search"
					placeholder="Search" aria-label="Search"> <input
					class="btn btn-outline-success my-2 my-sm-0" type="submit"
					value="Search">
			</form>
		</div>
	</nav>

	<div
		class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
		<h1 class="display-5">商品管理</h1>
		<p class="lead alert alert-danger" role="alert">※該当の商品は存在しません</p>
	</div>

	<div class="row">
		<div class="col-sm-2 border-right py-5">
			<div>
				<a href="MasterItemEditServlet"><button type="button"
						class="btn btn-primary btn-block">商品編集</button></a>
			</div>
			<div>
				<a href="MasterItemNewRegistServlet"><button type="button"
						class="btn btn-primary btn-block">商品登録</button></a>
			</div>
			<div>
				<a href="UserInfoServlet"><button type="button"
						class="btn btn-primary btn-block">Myページ</button></a>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="border container mb-5">
				<br>
				<h2>商品更新 削除</h2>
				<br>
				<div class="row">
					<div class="col-md">
						<form action="MasterItemEditServlet" method="post">
							<div class="form-group">
								<label>車種名：</label> <input type="text" class="form-control" name="carName">
							</div>
							<div class="form-group">
								<label>メーカー：</label> <select class="form-control" name="maker">
									<option value="ALL">全てのメーカー</option>
									<option value="トヨタ">トヨタ</option>
									<option value="ホンダ">ホンダ</option>
									<option value="日産">日産</option>
									<option value="三菱">三菱</option>
									<option value="スバル">スバル</option>
									<option value="スズキ">スズキ</option>
									<option value="BMW">BMW</option>
									<option value="キャデラック">キャデラック</option>
									<option value="シボレー">シボレー</option>
									<option value="ハマー">ハマー</option>
									<option value="リンカーン">リンカーン</option>
									<option value="ダッジ">ダッジ</option>
									<option value="フェラーリ">フェラーリ</option>
									<option value="ベンツ">ベンツ</option>
									<option value="アウディ">アウディ</option>
									<option value="MINI">MINI</option>
									<option value="Jeep">Jeep</option>
									<option value="ポルシェ">ポルシェ</option>
									<option value="ランボルギーニ">ランボルギーニ</option>
								</select>
							</div>
							<div class="form-group">
								<label>キーワード：</label> <input type="text" class="form-control" name="keyWord">
							</div>
							<div class="mb-4">
								<button type="submit" class="btn btn-outline-primary btn-block">検索</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="container border">
				<c:forEach var="mcl" items="${masterCarList}">
					<div class="row">
						<div class="col-md">
							<ul class="list-unstyled">
								<li class="media"> <img src="imag/${mcl.fileName}" alt="BMW" class="img-thumbnail" width="150" height="150">
									<div class="media-body">
										<h5 class="mt-0 mb-1">${mcl.carName}</h5>
										${mcl.detail}
										<p class="lead text-right">${mcl.price}円</p>
									</div>
									<div class="row bottom mb-5">
										<div class="col-sm-4">
											<a href="MasterItemUpdateServlet"><button type="submit" class="btn btn-info">詳細</button></a>
										</div>
										<div class="col-sm-4">
											<a href="MasterItemUpdateServlet?id=${mcl.id}"><button type="submit" class="btn btn-primary">更新</button></a>
										</div>
										<div class="col-sm-4">
											<a href="MasterItemDeleteServlet?id=${mcl.id}"><button type="submit" class="btn btn-danger">削除</button></a>
										</div>
									</div>
									</li>
							</ul>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

</body>

</html>