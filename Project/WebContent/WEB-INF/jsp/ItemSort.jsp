<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<title>国内車閲覧ページ</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="TopServlet">ショッピングサイト名</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="LoginServlet"><i class="fas fa-sign-in-alt"></i>ログイン <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UserNewRegistServlet"><i class="fas fa-registered"></i>新規登録</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserInfoServlet"><i class="fas fa-user"></i>Myページ</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-6" action="ItemSortServlet" method="post">
      <input type="text" name="search_word" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <a><button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Search</button></a>
    </form>
  </div>
</nav>

	<c:if test="${serchErrMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="text-align: right">
		  ${serchErrMsg}
		</div>
	</c:if>

<div>
	<div class="row rounded bg-dark">
	    <div class="mx-auto text-center col-sm">
			<img src="imag/エンブレム.jpg"  class="img-fluid" alt="エンブレム画像">
	    </div>
	    <div class="mx-auto text-center col-sm">
			<img src="imag/エンブレム.jpg"  class="img-fluid" alt="エンブレム画像">
	    </div>
	    <div class="mx-auto text-center col-sm">
			<img src="imag/エンブレム.jpg"  class="img-fluid" alt="エンブレム画像">
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-2 border-right py-5">
		●国産車：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">国内メーカー名</button></a></div>
	    ●輸入車：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">国外メーカー名</button></a></div>
	    ●排気量：
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">990cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">1,000cc～1,900cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">2,000cc～2,900cc以下</button></a></div>
	     <div><a href="ItemSortServlet"><button type="button" class="btn btn-info btn-block">3,000cc以上</button></a></div>
    </div>
    <div class="col-sm-10">
    	<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
			<h1 class="display-5 alert alert-info" role="alert">${mb.makerName}車</h1>
		</div>

			<c:forEach var="car" items="${makerList}">
			<div class="row featurette">
			      <div class="col-md-7 order-md-2">
			        <h2 class="featurette-heading">${car.carName}</h2>
			        <p class="lead">${car.detail}</p>
			        <p class="lead text-right">${car.price}円</p>
			      </div>
			        <a href="ItemDetailServlet"><img src="imag/${car.fileName}" alt="BMW" class="img-thumbnail" width="250" height="250"></a>
			</div>
			</c:forEach>

			<c:forEach var="car1" items="${displacementList}">
			<div class="row featurette">
			      <div class="col-md-7 order-md-2">
			        <h2 class="featurette-heading">${car1.carName}</h2>
			        <p class="lead">${car1.detail}</p>
			        <p class="lead text-right">${car1.price}円</p>
			      </div>
			        <a href="ItemDetailServlet"><img src="imag/${car1.fileName}" alt="BMW" class="img-thumbnail" width="250" height="250"></a>
			</div>
			</c:forEach>

			<c:forEach var="serchWord" items="${serchCarList}">
			<div class="row featurette">
			      <div class="col-md-7 order-md-2">
			        <h2 class="featurette-heading">${serchWord.carName}</h2>
			        <p class="lead">${serchWord.detail}</p>
			        <p class="lead text-right">${serchWord.price}円</p>
			      </div>
			        <a href="ItemDetailServlet"><img src="imag/${serchWord.fileName}" alt="BMW" class="img-thumbnail" width="250" height="250"></a>
			</div>
			</c:forEach>


    </div>
 						<div class="mx-auto text-center">
					      <nav aria-label="ページ切替">
						  <ul class="pagination align-text-bottom">
						    <li class="page-item disabled">
						      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
						    </li>
						    <li class="page-item"><a class="page-link" href="#">1</a></li>
						    <li class="page-item active" aria-current="page">
						      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
						    </li>
						    <li class="page-item"><a class="page-link" href="#">3</a></li>
						    <li class="page-item">
						      <a class="page-link" href="#">Next</a>
						    </li>
						  </ul>
						</nav>
						</div>
</div>
</body>
</html>